# TP ArbreTas

## Conditions de rendu

Ce TP est à effectuer seul·e et à rendre en fin de séance. Il sera noté et
comptera pour 10% de la note finale de l'UE. Pour réaliser ce TP, vous avez le
droit de consulter et récupérer le code de vos anciens TP. Vous avez également
le droit de naviguer sur internet et de consulter la documentation que vous
jugez utile. Vous êtes autorisés à utiliser toutes les fonctionnalités de la
librairie standard. Vous pouvez demander à votre encadrant une clarification du
sujet si vous estimez qu'il manque de clarté. Vous n'êtes par contre pas
autorisés à communiquer entre vous ou à échanger votre travail entre vous quelle
que soit la méthode.

Pour rendre le TP, soumettez une archive contenant l'intégralité de votre code
sur tomuss dans la case ouverte pour cette occasion. Votre archive doit être
dans un format ouvert simple : `zip` ou `tar.gz`. Vous n'avez pas besoin
d'inclure dans l'archive les binaires compilés, seul le code est nécessaire.

Le code fourni n'est pas forcément à préserver, vous êtes libres de modifier
tout ou partie de ce code pour correspondre à vos besoins : rajouter des
méthodes et des membres aux classes, des paramètres au méthodes, modifier le
main, ...

## ArbreTas

Un arbre tas est un arbre binaire contenant des couples (valeur, priorité). Du
point de vue des valeurs, l'arbre binaire est un arbre binaire de recherche,
c'est à dire que la valeur d'un nœud est plus grande que celles de ses
descendants gauches, et plus petite que celles de ses descendants droits. Les
valeurs égales peuvent être à droite ou à gauche. Du point de vue des priorités, 
l'arbre est un tas binaire, c'est à dire que la priorité d'un nœud est
supérieure à toutes celles de ses descendants, droits ou gauches. Par contre
l'arbre correspondant à ce tas binaire n'est pas un arbre complet rempli de
gauche à droite comme les tas binaires classiques implémentés dans un tableau et
vus en cours et en TD.

![exemple arbretas](Sujet/exemple_arbre.png)

Ici les valeurs sont en haut et les priorités en bas des nœuds.

On peut montrer qu'étant donné un ensemble de valeurs distinctes associées à des
priorités distinctes, il n'existe qu'un seul arbretas possible respectant
toutes les contraintes.

## Votre travail

### Aperçu global

Votre travail consiste à implémenter les arbretas à partir de la base de code
fournie (la même que pour le TP AVL). Cette implémentation nécessite d'utiliser
des rotations pour maintenir toutes les propriétés vérifiées. Vous réaliserez
d'abord l'insertion et la recherche dans un arbre tas. Vous rajouterez ensuite
la suppression d'une valeur.

### Premier arbretas

Modifiez la classe `Noeud` pour ajouter la notion de priorité, et l'insertion
pour qu'elle prenne en paramètre en plus une priorité. Ne cherchez pas pour
l'instant à modifier le mécanisme d'insertion, mais lorsque le nouveau nœud est
créé dans l'insertion, ajoutez lui la priorité demandée.  Modifiez
enfin la fonction d'affichage, en décommentant l'instruction qui affiche la
priorité, sous le commentaire `DECOMMENTEZ ICI`.

En insérant des nœuds avec des priorités aléatoire, votre arbre sera un arbre
binaire de recherche, mais pas un tas. Si par contre lors de l'insertion, vous
faites attention à insérer les valeurs par priorité décroissante, l'arbre obtenu
sera également un tas. Modifiez donc le `main` pour insérer les nœuds par
priorité décroissante, et vérifiez visuellement que votre arbre est bien un tas.

Cet arbre vous permet de traiter les questions suivantes dans l'ordre de votre
choix, et de tester les fonctionnalités sur un arbretas valide. Si vous êtes
coincés, n'hésitez donc pas à changer de question pour ne pas perdre de temps.

### Insertion

Pour réaliser l'insertion, vous modifierez la fonction d'insertion classique
fournie. Le principe consiste à dire qu'une insertion est d'abord réalisée comme
l'insertion classique dans un arbre binaire, afin de s'assurer que l'arbretas
respecte toujours les caractéristiques d'un arbre binaire. Une fois l'insertion
réalisée, en remontant la récursion le long du chemin d'insertion, chaque nœud 
vérifie que sa priorité est bien plus forte que celles de ses enfants. Si ce
n'est pas le cas, une rotation est réalisée pour y remédier. Vous aurez donc
besoin du code pour les rotations gauche et droite.

Si l'enfant gauche d'un nœud a une priorité plus forte que lui, une rotation à
droite permet de placer l'enfant gauche à la racine, et l'ancienne racine comme
l'un de ses enfants. De même si l'enfant droit d'un nœud a une priorité plus
forte que lui, une rotation à gauche permet d'y remédier. Notez que si
l'insertion qui vient d'être réalisée a été faite à gauche, le sous-arbre droit
n'a pas changé, et la priorité de l'enfant droit ne peut donc pas dépasser celle
de son parent. Après chaque insertion, il suffit donc de vérifier l'enfant du
côté de l'insertion.


[![exemple insertion](Sujet/exemple_insertion.gif)](Sujet/exemple_insertion.pdf)

Cliquez sur l'animation pour voir les étapes sans animation.

### Test tas binaire

Pour vérifier que l'arbre est bien un arbre binaire de recherche, une méthode
vous est fournie. En vous en inspirant, proposez une méthode permettant de
vérifier que l'arbre est bien également un tas binaire, c'est à dire que la
priorité d'un parent est plus forte que celles de ses descendants. Appelez la
dans le programme principal (le main) pour vous assurer que vos insertions vous
permettent bien d'obtenir à la fois un arbre binaire de recherche et un tas.

### Recherche

L'intérêt d'un arbre binaire de recherche est de pouvoir retrouver les valeurs
qu'il contient. Écrivez donc une méthode permettant de chercher une valeur dans
l'arbre. Cette méthode prendra en paramètre la valeur cherchée, et renverra
l'adresse du nœud la contenant. Lorsque la valeur cherchée n'est pas dans
l'arbre, l'adresse renvoyée devra être `nullptr`. Modifiez ensuite le programme
principal pour tester cette méthode. Vous vous assurerez que certaines valeurs
que vous aurez insérées manuellement sont bien présentes, et que certaines
valeurs dont vous savez qu'elles n'ont pas été insérées ne sont pas trouvées.

### Suppression

L'intérêt d'un tas binaire est de pouvoir l'utiliser comme file à priorité, et
donc de pouvoir en retirer l'élément de priorité maximale. Pour retirer un nœud
d'un arbre tas, le principe consiste à faire descendre ce nœud dans l'arbre
jusqu'en bas, et lorsqu'il est tout en bas et n'a plus d'enfant, le retirer.

Pour descendre un nœud d'un cran dans un arbre, vous utiliserez à nouveau une
rotation pour vous assurer que l'arbre reste un arbre binaire de recherche.
Une rotation gauche fera descendre le nœud à gauche, et mettra son enfant droit
comme nouvelle racine du sous-arbre. Une rotation droite descendra le nœud à
droite, et mettra son enfant gauche à la racine du sous-arbre. Pour que l'arbre
reste un tas binaire, il faut que la nouvelle racine ait la priorité maximale.
Il faut donc vérifier les priorités des deux enfants s'ils existent, et réaliser
la rotation pour faire en sorte que l'enfant de priorité maximale finisse à la
racine. Ainsi lorsque l'enfant gauche a une priorité plus forte que celle de
l'enfant droit, une rotation droite est effectuée. Lorsqu'un enfant n'existe
pas, la rotation fera en sorte de mettre l'autre enfant à la racine. Enfin
lorsqu'aucun enfant n'existe, le nœud est en bas de l'arbre et peut être
supprimé.

À la manière de la fonction interne récursive `inserer_noeud`, vous pourrez
utiliser une fonction récursive `supprimer_noeud` qui prend en paramètre
l'adresse du nœud à supprimer, et renvoie l'adresse de la racine du sous-arbre
après la suppression.  Cette fonction sera récursive : après avoir réalisé la
rotation permettant de descendre le nœud d'un cran, il faut rappeler la fonction
pour continuer à faire descendre le nœud jusqu'à ce qu'il soit en bas,
déclenchant le cas d'arrêt de la récursion.

[![exemple suppression](Sujet/exemple_suppression.gif)](Sujet/exemple_suppression.pdf)

Cliquez sur l'animation pour voir les étapes sans animation.
