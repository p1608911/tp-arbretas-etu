#pragma once

#include "noeud.hpp"

#include <iostream>

class Arbre {

  public :

    /** construction et destruction **/
    Arbre() ;
    ~Arbre() ;

    /** desactivation de la copie pour eviter les soucis **/
    Arbre(const Arbre&) = delete ;
    Arbre& operator&(const Arbre&) = delete ;

    /** insertion **/
    void inserer(int valeur) ;

    /** verification **/
    bool verifier_abr() ;

    /** affichage **/
    void afficher() ;

  private :

    Noeud* m_racine ;
} ;
