#include "arbre.hpp"

#include <iostream>
#include <limits>
#include <map>

Arbre::Arbre() : 
  //un arbre vide a une racine vallant nullptr
  m_racine(nullptr)
{}

Arbre::~Arbre() {
  //la racine se chargera de supprimer ses enfants
  delete m_racine ;
}

static Noeud* inserer_noeud(Noeud* n, int v) {
  if(n) {
    //le sous-arbre n'est pas vide
    if(v > n->valeur) {
      //insertion a droite
      n->droite = inserer_noeud(n->droite, v) ;
    } else {
      //insertion a gauche
      n->gauche = inserer_noeud(n->gauche, v) ;
    }
    //renvoi de la racine du sous-arbre apres insertion
    return n ;
  } else {
    //sous arbre vide, le remplacer par un nouveau noeud
    return new Noeud(v) ;
  }
}

void Arbre::inserer(int v) {
  //utilisation de la fonction recursive sur les noeuds
  m_racine = inserer_noeud(m_racine, v) ;
}

static bool verifier_abr_noeud(Noeud* n, int min_v, int max_v) {
  if(!n) {
    return true ; //l'arbre vide est correct
  }
  //verification que le noeud a une valeur entre les bornes
  if(n->valeur < min_v) {
    std::cout 
      << "la valeur du descendant droit "
      << n->valeur
      << " est plus petite que celle d'un aieul de valeur "
      << min_v
      << std::endl ;
    return false ;
  }
  if(n->valeur > max_v) {
    std::cout 
      << "la valeur du descendant gauche "
      << n->valeur
      << " est plus grande que celle d'un aieul de valeur "
      << max_v
      << std::endl ;
    return false ;
  }
  //verification des sous-arbres en ajustant les bornes autorisees
  return 
      verifier_abr_noeud(n->gauche, min_v, n->valeur) &&
      verifier_abr_noeud(n->droite, n->valeur, max_v)
      ;
}

bool Arbre::verifier_abr() {
  //lancement de la vérification sur la racine
  //pour l'instant toutes les valeurs sont autorisees
  return verifier_abr_noeud(
      m_racine, 
      std::numeric_limits<int>::min(), 
      std::numeric_limits<int>::max()
      ) ;
}

//decommentez la ligne suivante en cas de carracteres etranges dans le terminal
//#define AFFICHAGE_SIMPLE

#ifndef AFFICHAGE_SIMPLE

//carracteres de dessin des arbres utf8
static const char* SPLIT = "\xe2\x94\xa4" ;
static const char* V_BRANCH = "\xe2\x94\x82" ;
static const char* H_BRANCH = "\xe2\x94\x80" ;
static const char* UPPER_BRANCH = "\xe2\x95\xad" ;
static const char* LOWER_BRANCH = "\xe2\x95\xb0" ;

#else

//carracteres de dessin des arbres ASCII
static const char* SPLIT = "+" ;
static const char* V_BRANCH = "|" ;
static const char* H_BRANCH = "-" ;
static const char* UPPER_BRANCH = "+" ;
static const char* LOWER_BRANCH = "+" ;

#endif

static void afficher_noeud(Noeud* n, int profondeur, int code) {
  int largeur = 5 ;
  if(n) {
    //affichage du sous-arbre de droite
    afficher_noeud(n->droite, profondeur+1, code*2+1) ;
    int i ;
    //affichage ou non des lignes verticales des branches superieures
    for(i = 0; i < profondeur-1; ++i) {
      if(((code >> (profondeur-i-1)) & 1 ) != ((code >> (profondeur-i-2)) & 1)) {
        std::cout << V_BRANCH ;
      } else {
        std::cout << " " ;
      }
      for(int l = 0; l < largeur; ++l) {
        std::cout << " " ;
      }
    }
    //affichage du coude juste avant le noeud
    if(code%2) {
      std::cout << UPPER_BRANCH ;
    } else {
      if(profondeur) {
        std::cout << LOWER_BRANCH ;
      }
    }
    //affichage de la branche horizontale si pas la racine
    if(profondeur) {
      for(int l = 0; l < largeur; ++l) {
        std::cout << H_BRANCH ;
      }
    }
    //affichage du T renverse avant la valeur
    std::cout << SPLIT ;
    //affichage des informations du noeud

    //DECOMMENTEZ ICI
    
    std::cout << n->valeur /*<< " -- " << n->priorite*/ << std::endl ;


    //affichage du sous-arbre de gauche
    afficher_noeud(n->gauche, profondeur+1, code*2) ;
  }
}

void Arbre::afficher() {
  //appel de la fonction interne sur la racine
  afficher_noeud(m_racine, 0, 0) ;
}
