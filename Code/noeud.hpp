#pragma once

struct Noeud {

  //construction et destruction
  Noeud(int valeur) ;
  ~Noeud() ;

  //desactivation de la copie pour eviter les soucis
  Noeud(const Noeud&) = delete ;
  Noeud& operator=(const Noeud&) = delete ;

  //enfants
  Noeud* gauche ;
  Noeud* droite ;

  //valeur
  int valeur ;
} ;
